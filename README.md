# C++lex

## Introduction

At the very current stage the project is composed by three chunks:

* PILAL, the **P**athetically **I**nadequate **L**inear **A**lgebra **L**ibrary,
* Simplex, the actual simplex algorithm implementation, and
* the solver, a piece of software that instantiates Simplex.

Simplex depends on PILAL, while the solver depends on Simplex. The project can be built either under Windows (there is a MS Visual Studio 2008 solution which
can be automatically coverted to a 2010 solution without complications), Linux (make) or MacOS (make).

The project is currently working acceptably, however some optimizations and improvements might be added in the future:

  * optimization to exploit upper and lower limitations in variables,
  * *branch & bound* for integer linear programming,
  * multi-threading,
  * PILAL and Simplex compiled as libraries,
  * a more well-formed language to define problems (JSON maybe?),
  * Doxygen documentation
  
moreover, taking into account the new features of C++11, matrices should be re-implemented with move semantics.

## How to use it

All you have to do to compile the project is:

    make
    
If you want to clean up all the dependencies files and object files just type:

    make wipe
    
The "lib" directory was used in a previous Linux version to compile PILAL and Simplex as libraries, and is currently used by the Visual Studio solution to compile PILAL and Simplex as static libraries. Once you have compiled the project, to execute the solver with a problem file (look for some example problems in the "problems" directory) run under MacOS or Linux:

    ./solver <name_of_the_problem_file>
    
Under Windows:

    solver.exe <name_of_the_problem_file>
    
Problems have a very straightforward structure, therefore you can easily learn the syntax by example.

## Licensing

See LICENSE file.

## Drop a line

If you use this code for your software, please let me know, or not.
